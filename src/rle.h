#pragma once
#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <assert.h>
class rle
{
public:
    std::vector<unsigned char> rlEncode(std::vector<unsigned char> str);
    std::vector<unsigned char> str2CharVector(std::string stringInput);
    std::vector<unsigned char> str2AsciiVector(std::string stringInput);
    std::vector<unsigned char> rlDecode (std::vector <unsigned char> rldInput);
};