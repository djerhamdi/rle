#include "rle.h"

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {

        printf("ERROR - add filename as first argument \n");
        return -1;
    }

    rle obj;
    std::vector<unsigned char> datainput;
    std::vector<unsigned char> rleOutput;
    int length;

    FILE *f = fopen(argv[1], "r");
    if (f == NULL)
    {
        std::cout << "ERROR - failed to open file: " << argv[1] << std::endl;
        return -1;
    }

    // reading file
    fseek(f, 0, SEEK_END);
    length = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buffer = (char *)malloc(length);
    fread(buffer, 1, length, f);
    fclose(f);
    for (uint i = 0; i < length; i++)
    {
        datainput.push_back(buffer[i]);
    }
    std::cout << "datainput size = " << datainput.size() << std::endl;

    //rlEncoding
    rleOutput = obj.rlEncode(datainput);
    std::cout << "rle length = " << rleOutput.size() << std::endl;
    std::cout << "rlEncoding :" << std::endl;
    for (uint i = 0; i < rleOutput.size(); i++)
    {
        std::cout << ' ' << (int)rleOutput[i];
    }

    // rlDecde
    std::cout << std::endl;
    std::vector <unsigned char> rlDecode;
    rlDecode = obj.rlDecode(rleOutput);
    for (uint i = 0; i < rlDecode.size(); i++)
    {
        std::cout << ' ' << (int)rlDecode[i];
    }

    datainput [1] =1 ;
    //check if Rle works
    assert (rlDecode==datainput && "RLE not working properly - decoded file does not correspond to the initial file");
   
    free(buffer);
    return 0;
}