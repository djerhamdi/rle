#include "rle.h"

std::vector<unsigned char> rle::rlEncode(std::vector<unsigned char> rleInput)
{
  int size = rleInput.size();
  std::vector<unsigned char> rleOutput;
  for (uint i = 0; i < size; i++)
  {
    int count = 1;
    while (i < size - 1 && rleInput[i] == rleInput[i + 1])
    {
      count++;
      i++;
    }
    
    rleOutput.push_back(count  &  255);
    rleOutput.push_back(count >> 8 & 255);
    rleOutput.push_back(count >> 16 & 255);
    rleOutput.push_back(count >> 24 & 255);
    rleOutput.push_back(rleInput[i]);
  }

  return rleOutput;
}

std::vector<unsigned char> rle::rlDecode (std::vector <unsigned char> rldInput){

   std::vector<unsigned char> Rldecoded;
  int count = 0;
  for ( uint i=0; i <rldInput.size(); i=i+5){
    count = rldInput [i]+rldInput [i+1]+rldInput [i+2]+rldInput [i+3];
      for (uint j = 0; j< count ; j++){  
        Rldecoded.push_back(rldInput[i+4]);
      }
  }
  return Rldecoded;

}

std::vector<unsigned char> rle::str2CharVector(std::string stringInput)
{

  std::vector<unsigned char> charVector(stringInput.begin(), stringInput.end());

  return charVector;
}

std::vector<unsigned char> rle::str2AsciiVector(std::string stringInput)
{
  char char2ascii;
  std::vector<unsigned char> asciiVector;
  for (uint i = 0; i < stringInput.length(); i++)
  {
    char x = stringInput.at(i);
    char2ascii = int(x) + '0';
    asciiVector.push_back(char2ascii);
  }

  return asciiVector;
}